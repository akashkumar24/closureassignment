function limitFunctionCallCount(cb, n) {
  let callCount = 0;

  if(typeof cb!== 'function'){
    throw new Error('the cb must be a function');
  }
  if(typeof n!== 'number' || n <1){
    throw new Error('The n must be a positive number');
  }

  return function() {
    if (callCount < n) {
      callCount++;
      return cb.apply(this, arguments);
    } else {
      console.log('Max call count reached');
      return null;
    }
  };
}

module.exports=limitFunctionCallCount;

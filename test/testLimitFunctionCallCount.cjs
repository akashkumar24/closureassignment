const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');

function welcome(name) {
    console.log('Welcome, ' + name + '!');
  }
  
  var lwelcome = limitFunctionCallCount(welcome, 2);

  try {
    var lwelcome = limitFunctionCallCount(welcome);
  } catch (error) {
    console.error(error.message); // The n must be a positive number
  }
  
  try {
    var lwelcome = limitFunctionCallCount(welcome, -1);
  } catch (error) {
    console.error(error.message); // The n must be a positive number
  }
  
  try {
    var lwelcome = limitFunctionCallCount('hello', 2);
  } catch (error) {
    console.error(error.message); // the cb must be a function
  }
  
  
  lwelcome('Akash'); 
  lwelcome('Pallavi'); 
  lwelcome('preeti'); 
  

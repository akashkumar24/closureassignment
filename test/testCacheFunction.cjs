const cacheFunction = require('../cacheFunction.cjs');

// Define a function to be cached
const add = (a, b) => a + b;

  const cachedFunction = cacheFunction(add);
  try {
    const cachedFunction = cacheFunction();
  } catch (error) {
    console.error(error.message); // The cb must be a function
  }
  
  try {
    const cachedFunction = cacheFunction('hello');
  } catch (error) {
    console.error(error.message); // The cb must be a function
  }
  
  
  console.log(cachedFunction(1, 2)); //3
  console.log(cachedFunction(1, 2)); //  3 (read from cache)
  console.log(cachedFunction(2, 3)); // 5
  console.log(cachedFunction(2, 3)); // 5 (read from cache)

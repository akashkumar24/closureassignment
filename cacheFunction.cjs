function cacheFunction(cb) {
  if (typeof cb !== 'function') {
    throw new Error('The cb must be a function');
  }
  
    const cache = {};
    return function(...args) {
      const argStr= JSON.stringify(args);
      if (argStr in cache) {
        return cache[argStr];
      } else {
        const result = cb(...args);
        cache[argStr] = result;
        return result;
      }
    }
  }

  module.exports = cacheFunction;
  